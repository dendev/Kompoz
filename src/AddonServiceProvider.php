<?php

namespace Dendev\Kompoz;

use Dendev\Kompoz\Console\Commands\Install;
use Dendev\Kompoz\Console\Commands\Make;
use Dendev\Kompoz\Console\Commands\MakePageController;
use Illuminate\Support\ServiceProvider;

class AddonServiceProvider extends ServiceProvider
{
    use AutomaticServiceProvider;

    protected $vendorName = 'dendev';
    protected $packageName = 'kompoz';
    protected $commands = [Install::class, Make::class, MakePageController::class];
}
