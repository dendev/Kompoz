<?php
namespace Dendev\Kompoz\Facades;

use Illuminate\Support\Facades\Facade;

class KompozManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'kompoz_manager';
    }
}
