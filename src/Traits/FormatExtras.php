<?php


namespace Dendev\Kompoz\Traits;


trait FormatExtras
{
    private function _format_extras($extras, $request)
    {
        if( $extras ) // form get mth
        {
            if( is_string($extras) ) // it's json
            {
                $extras = json_decode($extras, true);
            }

            if( ! is_array($extras) )
            {
                $tmp = $extras;
                $extras = [];
                $extras[] = $tmp;
            }
        }
        else // from post mth
        {
            $extras = $request->input();
            if( $request->hasFile('file') )
            {
                $extras['file'] = $request->file('file');
            }
        }

        // auth datas
        $extras['auth']['token'] = str_replace('Bearer ', '', $request->headers->get('authorization') );
        //$extras['auth']['identity'] = $request->headers->get('identity');

        // user datas
        $user = $this->tokenToUser($request);
        $extras['user'] = $user;

        return $extras;
    }
}
