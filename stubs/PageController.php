<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\Request;

class PageController extends Controller
{
    public function home(Request $request)
    {
        return view('pages.home');
    }
}
