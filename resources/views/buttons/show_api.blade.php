<?php
?>
<a href="javascript:void(0)" onclick="showApi(this)"
   data-route="{{route('kompoz.show_api', ['kompoz_id' => $entry->id])}}"
   data-button-type="showApi"
   class="btn btn-sm btn-link">
    <i class="la la-code"></i> {{trans('dendev.kompoz::kompoz.show_api_action')}}
</a>

<script>
    if (typeof showApi != 'function') {
        var fct = document.querySelector("[data-button-type=show_api]")
        document.removeEventListener('click', fct);

        function showApi(button) {
            var button = $(button);
            var route = button.attr('data-route');

            console.log( route);
            window.location.href = route;
        }
    }
    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('deleteEntry');
</script>
