import React from "react";
import {initReactI18next, Trans} from 'react-i18next';
import i18n from "i18next";
import {Alert, Spinner} from "react-bootstrap";


export default class Kompoz extends React.Component
{
    constructor(props)
    {
        super(props);

        // read settings
        let settings = this._check_settings(props);

        // set name
        this.component_name = settings.component_name;

        // get datas
        this._get_values_from_remote(settings);
    }

    // Renders
    render()
    {
        if( this.state.is_loading === undefined || this.state.is_loading )
            return this._render_loading();
        else if( this.state.alert && this.state.alert.is_active )
            return this._render_alert();
        else
            return this.render_component();
    }

    _render_loading()
    {
        return (
            <div className="d-flex justify-content-around align-items-center" id="page-loading">
                <div role="status" id="page-loading-spinner">
                    <Spinner animation={"border"}/>
                </div>
            </div>
        );
    }

    _render_alert()
    {
        return (
            <div className="flex-center" id="page-alert">
                <Alert
                    variant={"warning"}
                    id="page-alert-msg"
                >
                    {this.state.alert.msg}
                </Alert>
            </div>
        );
    }

    // Texts
    gtoc(text_id)
    {
        return this.get_text_of_component(text_id);
    }

    get_text_of_component(text_id)
    {
        return <Trans ns={this.component_name}>{text_id}</Trans>
    }

    // Datas
    gvod(data_id)
    {
        return this.get_value_of_data(data_id);
    }

    get_value_of_data(data_id)
    {
        let value = null;
        let datas = this.datas;

        if( datas && typeof datas === 'object' )
        {
            if( datas.hasOwnProperty(data_id))
            {
                if( datas[data_id] ) // false === null style
                {
                    value = datas[data_id]
                }
            }
            else
            {
                console.error(`[Kompoz::get_value_of_data] Kgvod02 : ${data_id} not found ! `);
            }
        }
        else
        {
            console.error(`[Kompoz::get_value_of_data] Kgvod01 : no datas provided! `);
        }

        return value;
    }

    // Styles
    gsoc(class_name)
    {
        return this.get_style_of_class(class_name);
    }

    get_style_of_class(class_name)
    {
        let style = null;
        let styles = this.styles;

        if( styles && typeof styles === 'object' )
        {
            if( styles.hasOwnProperty(class_name))
            {
                if( styles[class_name] ) // false === null style
                {
                    style = styles[class_name]
                }
            }
            else
            {
                console.error(`[Style::get_style_of_class] Sgsoc02 : ${class_name} not found ! `);
            }
        }
        else
        {
            console.error(`[Style::get_style_of_class] Sgsoc01 : no styles provided! `);
        }

        return style;
    }

    // -
    _check_settings(props)
    {
        let settings;

        if(props && typeof  props === 'object' && props.hasOwnProperty('settings') )
        {
            settings = props.settings;
            if ( typeof settings === 'object' && settings.hasOwnProperty('providers'))
            {
                let providers = settings.providers;
                if (!providers.hasOwnProperty('texts'))
                    console.error(`[Kompoz::init] Ki02: Provider for texts not found`);
            }
            else
            {
                console.error(`[Kompoz::init] Ki01: Settings or providers not found`);
            }
        }
        else
        {
            console.error(`[Kompoz::init] Ki00: Settings not found`);
        }

        return settings;
    }

    _get_values_from_remote(settings)
    {
        let providers = settings.providers;

        if( providers.is_enabled )
        {
            if( providers.is_same_provider )
            {
                if( providers.texts.rest_api )
                    this._remote_get_all(providers);
                else
                    console.error(`[Kompoz::_get_values_from_remote] K_gvfr01: no api url found in provider`);
            }
            else
            {
                // texts
                if( providers.texts.rest_api )
                    this._remote_get_ressource(providers.texts, 'texts');
                else
                    console.error(`[Kompoz::_get_values_from_remote] K_gvfr02: no api url found in provider texts`) ;

                // datas
                if( providers.datas.rest_api )
                    this._remote_get_ressource(providers.texts, 'datas');
                else
                    console.error(`[Kompoz::_get_values_from_remote] K_gvfr02: no api url found in provider datas`) ;

                // styles
                if( providers.styles.rest_api )
                    this._remote_get_ressource(providers.texts, 'styles');
                else
                    console.error(`[Kompoz::_get_values_from_remote] K_gvfr02: no api url found in provider styles`) ;
            }
        }
    }

    _remote_get_all(providers)
    {
        let that = this;
        let url = providers.texts.rest_api;
        fetch(url)
            .then(response => {
                if(  response.status !== 200 )
                {
                    throw new Error("HTTP error, status = " + response.status);
                }

                return response.json();
            })
            .then( data => {
                // get from response
                let result = data.data[that.component_name];

                // set in Kompoz
                that.texts = result.texts;
                that.datas = ( result.datas ) ? result.datas : null;
                that.styles = result.styles;

                // add to translator
                that._init_i18next();
            })
            .catch(error => {
                console.error(`[Kompoz::_get_value_from_remote] K_gvfr01 error on fetch ${error}`);
                let alert = {
                    is_active: true,
                    msg: 'Erreur lors de la récupération de données',
                    type: 'error',
                };
                this.setState({ alert : alert });
            })
            .finally(() => {
                this.setState({is_loading: false})
            });
    }

    _remote_get_ressource(provider, type)
    {
        let that = this;
        let url = provider.rest_api;
        fetch(url)
            .then(response => {
                if(  response.status !== 200 )
                {
                    throw new Error("HTTP error, status = " + response.status);
                }

                return response.json();
            })
            .then( data => {
                // get from response
                let result = data.data[that.component_name];

                // set in Kompoz
                that[type] = result[type];
                console.log( result[type]);

                // add to translator
                that._init_i18next();
            })
            .catch(error => {
                console.error(`[Kompoz::_get_value_from_remote] K_gvfr01 error on fetch ${error}`);
                let alert = {
                    is_active: true,
                    msg: 'Erreur lors de la récupération de données',
                    type: 'error',
                };
                this.setState({ alert : alert });
            })
            .finally(() => {
                this.setState({is_loading: false})
            });
    }

    _init_i18next()
    {
        let texts = this.texts;

        i18n
            .use(initReactI18next) // passes i18n down to react-i18next
            .init({
                resources: texts,
                lng: "en", // language to use, more information here: https://www.i18next.com/overview/configuration-options#languages-namespaces-resources
                // you can use the i18n.changeLanguage function to change the language manually: https://www.i18next.com/overview/api#changelanguage

                interpolation: {
                    escapeValue: false // react already safes from xss
                }
            });
    }
}
