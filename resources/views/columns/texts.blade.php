@php
$texts = ( count( $entry->{$column['name']} ) > 0 ) ? $entry->{$column['name']} : false;
@endphp

@if( $texts )
    <span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <thead>
        <tr>
            <th>{{trans('dendev.kompoz::kompoz.field_text_label')}}</th>
            <th>{{trans('dendev.kompoz::kompoz.field_text_value')}}</th>
            <th>{{trans('dendev.kompoz::kompoz.field_text_lang')}}</th>
            <th>{{trans('dendev.kompoz::kompoz.field_text_description')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($texts as $text)
        <tr>
            <td>
                {{$text['key']}}
            </td>
            <td>
                {{$text['value']}}
            </td>
              <td>
                {{$text['lang']}}
            </td>
            <td>
                {{$text['description']}}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </span>
@else

@endif


