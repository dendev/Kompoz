<?php

namespace Dendev\Kompoz\Http\Controllers\Api;

use Dendev\Kompoz\Traits\FormatExtras;
use Illuminate\Http\Request;

class KompozApiController extends ApiController
{
    use FormatExtras;

    /**
     * @OA\Get(
     *     path="/api/front",
     *     description="provid front datas",
     *     @OA\Response(response=200, description="success: get datas"),
     *     @OA\Response(response=202, description="empty: no datas"),
     * )
     */
    public function action(Request $request, $identity, $action, $extras = [])
    {
        $extras = $this->_format_extras($extras, $request);

        $datas = \KompozManager::run($identity, $action, $extras);

        return $this->sendResponse($datas);
    }
}
