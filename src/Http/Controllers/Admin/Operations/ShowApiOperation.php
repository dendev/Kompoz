<?php

namespace Dendev\Kompoz\Http\Controllers\Admin\Operations;

use Dendev\Kompoz\Models\Kompoz;
use Illuminate\Support\Facades\Route;

trait ShowApiOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupShowApiRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/show_api/{kompoz_id}', [
            'as'        => $routeName.'.show_api',
            'uses'      => $controller.'@show_api',
            'operation' => 'show_api',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupShowApiDefaults()
    {
        //$this->crud->allowAccess('show_api');

        $this->crud->operation('show_api', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'show_api', 'view', 'dendev.kompoz::buttons.show_api');
        });

        $this->crud->operation('show', function () {
            $this->crud->addButton('line', 'run', 'view', 'dendev.kompoz::buttons.show_api');
        });
    }

    public function show_api($kompoz_id)
    {
        $kompoz = Kompoz::find($kompoz_id);// route('kompoz.show_api', ['kompoz_id' => $kompoz_id]);

        $url = route('kompoz.action', ['identity' => $kompoz->identity, 'action' => 'get']);

        return \Redirect::to($url);
    }


}
