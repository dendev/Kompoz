<?php
namespace Dendev\Kompoz\Services;

use App\Services\Getter;
use Dendev\Kompoz\Models\Kompoz;
use Dendev\Kompoz\Traits\UtilService;
use Illuminate\Support\Str;

class KompozManagerService
{
    use UtilService;

    private $_available_actions;

    public function __construct()
    {
    }

    public function create($label, $texts, $actions, $styles, $identity = false, $description = null, $is_enabled = true)
    {
        if( ! $identity )
            $identity = Str::slug($label);

        if( ! is_array($texts) )
            $texts = [];

        if( ! is_array($actions) )
            $actions = [];

        if( ! is_array($styles) )
            $styles = [];

        $already_exist = Kompoz::where('identity', $identity)->first();
        if( $already_exist )
        {
            $kompoz = $already_exist;
        }
        else
        {
            $kompoz = new Kompoz();
        }

        $kompoz->label = $label;
        $kompoz->description = $description;
        $kompoz->texts = $texts;
        $kompoz->actions = $actions;
        $kompoz->styles = $styles;
        $kompoz->is_enabled = $is_enabled;
        $kompoz->save();

        return $kompoz;
    }

    public function do_action_of_kompoz_by_identity($identity, $action_name, $extras)
    {
        $datas = false;

        $kompoz = Kompoz::where('identity', $identity)->first();
        if( $kompoz)
        {
            $datas = $this->do_action_of_kompoz($kompoz, $action_name, $extras);
        }
        else
        {
            \Log::error("[Kompoz::KompozManagerService::action] No Kompoz with this identity", [
                'identity' => $identity
            ]);
        }

        return $datas;
    }

    public function do_action_of_kompoz($id_or_model_kompoz, $action_name, $extras)
    {
        $datas = false;

        $kompoz = $this->_instantiate_if_id($id_or_model_kompoz, Kompoz::class);
        if( $kompoz )
        {
            $action = \KompozManager::get_action_of_kompoz($kompoz, $action_name);

            // find class to do action
            $class_and_method = $this->_get_classname_and_method($action);

            if( is_array($class_and_method) )
            {
                $classname = $class_and_method['classname'];
                $method = $class_and_method['method'];
                if( ! class_exists($classname) )
                {
                    \Log::error("[Kompoz::KompozManagerService::action] Class not found", [
                        'classname' => $classname
                    ]);
                }
                else if( ! method_exists($method, $classname) )
                {
                    \Log::error("[Kompoz::KompozManagerService::action] Method not found", [
                        'method' => $method,
                        'classname' => $classname
                    ]);
                }
                else
                {
                    // instantiate
                    $object = new $classname; // try catch

                    // do action
                    $datas = $object->$method($extras);
                }
            }
        }
        else
        {
            \Log::error("[Kompoz::KompozManagerService::action] No Kompoz found", [
                'id_or_model' => $id_or_model_kompoz
            ]);
        }

        return $datas;
    }

    public function get_action_of_kompoz($id_or_model_kompoz, $action_key)
    {
        $getter = false;

        $kompoz = $this->_instantiate_if_id($id_or_model_kompoz, Kompoz::class);
        if( $kompoz )
        {
            if( $kompoz->actions && count( $kompoz->actions) > 0 )
            {
                $actions = $kompoz->actions;
                foreach( $actions as $action)
                {
                    if( $action['key'] === $action_key )
                        $getter = $action['value'];
                }
            }
            if( ! $getter)
                \Log::warning("[Kompoz::KompozManagerService::get_action_of_kompoz] No action found", [
                    'action_key' => $action_key,
                    'kompoz' => $kompoz
                ]);
        }

        return $getter;
    }

    public function run($identity, $action_name, $extras)
    {
        $formated = false;

        $kompoz = Kompoz::where('identity', $identity)->first();
        if( $kompoz )
        {
            // get datas
            $datas = $this->do_action_of_kompoz($kompoz, $action_name, $extras);
            $kompoz->datas = $datas;

            // format
            $formated = $this->_format($kompoz);
        }
        else
        {
            \Log::error("[Kompoz::KompozManagerService::run] No Kompoz found for the identity", [
                'identity' => $identity
            ]);
        }


        return $formated;
    }

    public function test_me()
    {
        return true;
    }

    // -
    private function _format($kompoz)
    {
        unset($kompoz->actions);

        $texts_formated = $this->_format_texts($kompoz);
        $datas_formated = $this->_format_datas($kompoz);
        $styles_formated = $this->_format_styles($kompoz);

        $values = [
            'label' => $kompoz->label,
            'identity' => $kompoz->identity,
            'description' => $kompoz->description,
            'texts' => $texts_formated,
            'datas' => $datas_formated,
            'styles' => $styles_formated,
        ];

        return $values;
    }

    private function _format_texts($kompoz)
    {
        $formated = [];

        if( $kompoz->texts && is_array($kompoz->texts) && count( $kompoz->texts ) > 0 )
        {
            $identity = $kompoz->identity;
            $texts = $kompoz->texts;
            foreach( $texts as $text )
            {
                if( array_key_exists('key', $text) && array_key_exists('value', $text) && array_key_exists('lang', $text))
                {
                    $key = $text['key'];
                    $value = $text['value'];
                    $lang = $text['lang'];

                    $description = ( array_key_exists('description', $text) ) ? $text['description'] : null;

                    $row = [$key => $value];
                    $formated[$lang][$identity] = $row;
                }
                else
                {
                    \Log::warning("[Kompoz::KompozManagerService::_format_texts] Text mal formated", [
                        'text' => $text,
                        'texts' => $texts
                    ]);
                }
            }
        }

        return $formated;
    }

    private function _format_datas($kompoz)
    {
        $formated = [];

        if( $kompoz->datas && is_array($kompoz->datas) && count( $kompoz->datas ) > 0 )
        {
           $formated = $kompoz->datas;
        }

        return $formated;
    }

    private function _format_styles($kompoz)
    {
        $formated = [];

        if( count( $kompoz->styles_as_array ) > 0 )
        {
            $identity = $kompoz->identity;
            $styles = $kompoz->styles_as_array;
            foreach( $styles as $style )
            {
                if( array_key_exists('key', $style) && array_key_exists('value', $style) )
                {
                    $key = $style['key'];
                    $value = $style['value'];

                    $description = ( array_key_exists('description', $style) ) ? $style['description'] : null;

                    $formated[$key] = $value;
                }
                else
                {
                    \Log::warning("[Kompoz::KompozManagerService::_format_styles] Style mal formated", [
                        'style' => $style,
                        'styles' => $styles
                    ]);
                }
            }
        }


        return $formated;
    }

    private function _get_classname_and_method($action)
    {
        $ok = false;

        $tmp = explode('::', $action);
        if( count( $tmp ) === 2 )
        {
            $ok = [
                'classname' => $tmp[0],
                'method' => $tmp[1],
            ];
        }
        else
        {
            \Log::error("[Kompoz::KompozManager::_get_classname_and_method] No valid action. Must be like full_classname::method.", [
                'action' =>$action,
            ]);
        }

        return $ok;
    }
}
