<?php

namespace Dendev\Kompoz\Tests\Unit\Mocks;


class Getter
{
    public function get_1()
    {
        // do complex action to get actual value
        return ['field_a' => 'a'];
    }

    public function get_2()
    {
        return 99;
    }
}
