# Kompoz

> provides datas to react kompoz


## Install
```bash
composer config repositories.kompoz vcs https://gitlab.com/dendev/kompoz.git
composer require dendev/kompoz

# sans publication des fichiers langs et config
php artisan kompoz:install 

# avec publication des fichiers langs et config
php artisan kompozit:install --publish
```

## Config

Add mix var in .env.  
This values are used by component who need api interactions.  
```bash
MIX_KOMPOZ_API_URL=http://larapack.local/api/kompoz
MIX_KOMPOZ_API_BEARER=todo doc about token
```

Publish config file of package ( actualy empty )
```bash
 php artisan vendor:publish --provider="Dendev\Kompoz\AddonServiceProvider" --tag=config
```

## Lang

Publish lang files of package 
```bash
 php artisan vendor:publish --provider="Dendev\Kompoz\AddonServiceProvider" --tag=lang
```

## Usage
create page controller
```bash
php artisan php artisan kompoz:make_page_controller
```

create a new kompoz component
```bash
php artisan php artisan kompoz:make home Pages --blade  
```

add methode home in app/Http/PageController.   
edit app/Services/Kompozs/Home to add methode who get datas.   
edit resources/js/kompozs/Pages/Home.js to display datas.   

create a new kompoz component without blade page
```bash
php artisan php artisan kompoz:make home Pages --blade  
```

