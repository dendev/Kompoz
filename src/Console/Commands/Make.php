<?php

namespace Dendev\Kompoz\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class Make extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kompoz:make {name : name of the futur component} {type : kind of component} {--blade : create blade page for component}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create an integrated kompoz ( front && back )';

    /**
     * Get the stub file for the generator.
     *
     * @return array
     */
    protected function getStub()
    {
        return [
            'js' => __DIR__ . '/../../../stubs/Component.js',
            'worker' => __DIR__ . '/../../../stubs/Worker.php',
            'blade' => __DIR__ . '/../../../stubs/component.blade.php',
            ];
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {
        $this->_make_worker();
        $this->_make_js();
        $this->_create_in_db();
        $this->_make_blade();
    }

    private function _make_worker()
    {
        $blade = $this->option('blade');

        if( $blade)
        {
            $this->info('');
            $this->info('* Make worker');
            $name = $this->argument('name');
            $name = ucfirst($name);

            $type = $this->argument('type');
            $type = ucfirst($type);

            $destination_path = $this->laravel['path'] . "/Services/Kompozs/$name.php";

            if ($this->files->exists($destination_path))
            {
                $this->error('-- Kompoz worker already exists!');

                return false;
            }

            $this->makeDirectory($destination_path);

            $this->files->put($destination_path, $this->buildWorkerFile($name, $type));

            $this->info("++ Kompoz worker $name created successfully.");
            $this->info("?? Edit $destination_path add method who return array of datas.");
        }
    }

    private function _make_js()
    {
        $this->info('');
        $this->info('* Make js');
        $name = $this->argument('name');
        $name = ucfirst($name);
        $name_no_ext = str_replace('.js', '', $name);

        $type = $this->argument('type');
        $type = ucfirst($type);

        $destination_path = $this->laravel['path']."/../resources/js/kompozs/$type/$name.js";

        if ($this->files->exists($destination_path)) {
            $this->error('-- Kompoz already exists!');

            return false;
        }

        $this->makeDirectory($destination_path);

        $this->files->put($destination_path, $this->buildJsFile($name, $type));

        $this->info("++ Kompoz $name created successfully.");
        if( $this->option('blade'))
        {
            $this->info("?? Edit resources/js/kompozs/ui.js");
            $this->line("require('./$type/$name_no_ext.);");
        }
        $this->info("?? Edit resources/js/kompozs/$type/$name.js to creater render");
    }

    private function _create_in_db()
    {
        $this->info('');
        $this->info('* Create in DB');
        $name = $this->argument('name');
        $name = ucfirst($name);

        $kompoz = \KompozManager::create($name, [], [], []);

        if( $kompoz )
            $this->info('++ Done');
        else
            $this->info("++ Error can't create $name in db");
    }

    private function _make_blade()
    {
        $blade = $this->option('blade');

        if( $blade)
        {
            $this->info('');
            $this->info('* Make blade');

            $name = $this->argument('name');
            $name_lo = strtolower($name);
            $name = ucfirst($name);

            $type = $this->argument('type');
            $type_lo = strtolower($type);
            $type = ucfirst($type);

            $destination_path = $this->laravel['path'] . "/../resources/views/$type_lo/$name_lo.blade.php";

            if ($this->files->exists($destination_path)) {
                $this->error('-- Kompoz blade already exists!');

                return false;
            }

            $this->makeDirectory($destination_path);

            $this->files->put($destination_path, $this->buildBladeFile($name, $type));

            $this->info("++ Kompoz blade $name created successfully.");
            $this->info("?? Edit app/AHttp/Controllers/PageController and add : ");
            $this->line("
               public function $name(Request \$request)
                {
                    return view('$type_lo.$name');
                }
            ");

            $this->info("?? Edit web.php and add : ");
            $this->line("Route::get('$name', [PageController::class, '$name'])->name('$type_lo.$name');");
        }
    }

    /**
     * Build the class. Replace Leodel namespace with App one.
     *
     * @param string $name
     *
     * @return string
     */
    protected function buildJsFile($name, $type)
    {
        $stub = $this->files->get($this->getStub()['js']);

        return $this->makeJsReplacements($stub, $name, $type);
    }

    protected function buildWorkerFile($name, $type)
    {
        $stub = $this->files->get($this->getStub()['worker']);

        return $this->makeWorkerReplacements($stub, $name, $type);
    }

    protected function buildBladeFile($name, $type)
    {
        $stub = $this->files->get($this->getStub()['blade']);

        return $this->makeWorkerReplacements($stub, $name, $type);
    }

    /**
     * Replace the namespace for the given stub.
     * Replace the User model, if it was moved to App\Models\User.
     *
     * @param string $stub
     * @param string $name
     *
     * @return string|string[]
     */
    protected function makeJsReplacements(&$stub, $name, $type)
    {
        $stub = str_replace('Component', $name, $stub);
        $stub = str_replace('identity', strtolower($name), $stub);

        $name_lo = strtolower($name);

        $stub .= "const root = document.getElementById('$name_lo');
if (root) {
    ReactDOM.render(<$name/>, root);
}";

        return $stub;
    }

    protected function makeWorkerReplacements(&$stub, $name, $type)
    {
        //$stub = str_replace('Dendev\Report\Console\Commands;', $this->laravel->getNamespace().'Console\Commands;', $stub);
        $stub = str_replace('Worker', $name, $stub);
        $stub = str_replace('identity', strtolower($name), $stub);

        return $stub;
    }

    protected function makeBladeReplacements(&$stub, $name, $type)
    {
        $stub = str_replace('Component', $name, $stub);
        $stub = str_replace('identity', strtolower($name), $stub);

        return $stub;
    }
}
