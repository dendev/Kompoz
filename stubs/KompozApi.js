import axios from 'axios';

// config
const kompozApi = axios.create({
    baseURL: process.env.MIX_APP_API_URL
});

function get_config()
{
    let config = null;

    config = {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Item-Type': item_type,
        }
    };

    let token = ( window.user_token ) ? window.user_token : '';
    if( token )
    {
        config.headers['Authorization'] = `Bearer ${token}`;
    }

    return config;
}
// Component
// TODO admin
kompozApi.component_action = function(identity, action, extras, success_callback, no_content_callback, error_callback, unauthorized_callback)
{
    // choose get || post && make url
    let promise;

    let unauthorized_msg;
    let no_content_msg;
    let error_msg;
    if( action === 'save' )// TODO user save-action_name
    {
        let url = `/kompoz/${identity}/${action}`;

        promise = kompozApi.post(url, { datas: extras }, get_config() );

        no_content_msg = `Aucune donnée trouvée pour le composant: ${identity}!`;
        unauthorized_msg = `Accès non authorisé, session expirée`;
        error_msg = `Erreur lors de l'envoi des données pour le composant: ${identity}`;
    }
    else
    {
        let url = `/kompoz/${identity}/${action}`;
        if( extras )
            if( extras instanceof Object )
                url = `/kompoz/${identity}/${action}/${JSON.stringify(extras)}`;
            else
                url = `/kompoz/${identity}/${action}/${extras}`;

        promise = kompozApi.get(url, get_config() );

        no_content_msg = `Aucune donnée trouvée pour le composant: ${identity}!`;
        unauthorized_msg = `Accès non authorisé, session expirée`;
        error_msg = `Erreur lors de la récupération des données pour le composant: ${identity}`;
    }

    // execute
    promise.then(res => {
        console.log( 'RES API');
        console.log( res.data );
        if( res.status === 200 )
        {
            let item = {};

            let datas = res.data.data;
            for (const [key, value] of Object.entries(datas))
            {
                item[key] = value;
            }
            success_callback( item );
        }
        else
        {
            no_content_callback(no_content_msg);
            console.info('no datas found for request domains');
        }
    })
        .catch(function (error) {
            console.log( error);
            if( error.response.status === 303 )
            {
                let url = error.response.data.data.redirect_to;
                let win = window.open(url, '_self');
                win.focus();
            }
            else if( error.response.status === 401 )
            {
                console.error(error);
                unauthorized_callback(unauthorized_msg);
            }
            else
            {
                console.error(error);
                error_callback(error_msg);
            }
        });
};

export default kompozApi;
