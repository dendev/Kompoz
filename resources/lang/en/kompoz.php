<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dendev\Kompoz Translation Lines
    |--------------------------------------------------------------------------
    */

    // fields
    'field_label'    => 'label',
    'field_slug'    => 'slug',
    'field_description'    => 'description',

    'field_texts'    => 'texts',
    'field_text_label'    => 'label',
    'field_text_value'    => 'value',
    'field_text_lang'    => 'lang',
    'field_text_description'    => 'description',
    'field_text_add'    => 'add text',

    'field_actions'    => 'actions',
    'field_action_key'    => 'label',
    'field_action_value'    => 'value',
    'field_action_description'    => 'description',
    'field_action_add'    => 'add action',

    'field_styles'    => 'styles',
    'field_style_key'    => 'label',
    'field_style_value'    => 'value',
    'field_style_description'    => 'description',
    'field_style_add'    => 'add style',

    'field_is_enabled'    => 'is enabled',
    'field_is_enabled_yes'    => 'yes',
    'field_is_enabled_no'    => 'no',


    // validations
    'validation_required' => 'The :attribute field is required.',

    // buttons
    'show_web.action' => 'Show web',
    'show_api.action' => 'Show api',

    // operations
    'operation_show_web_no' => 'no view to show',

    'operation_show_api_no' => 'no api to show',

    // buttons

];
