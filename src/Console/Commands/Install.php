<?php

namespace Dendev\Kompoz\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class Install extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kompoz:install {--p|publish : publish config and lang files}';

    /**
     * The console command description.
     *
     *
     * @var string
     */
    protected $description = 'Install Kompoz package';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->publish_config();
        $this->publish_lang();

        $this->migrate();
        $this->install_js();
        $this->directories_js();
        $this->install_page_controller();

        $this->about_usage();
    }

    // Publish
    public function publish_config()
    {
        if( $this->option('publish') )
        {
            $this->call('vendor:publish', [
                '--provider' => 'Dendev\Kompoz\AddonServiceProvider',
                '--tag' => 'config'
            ]);

            $this->info('[Kompoz] config published in config/dendev/kompoz/');

            $this->about_config();
        }
    }

    public function publish_lang()
    {
        if( $this->option('publish') === true)
        {
            $this->call('vendor:publish', [
                '--provider' => 'Dendev\Kompoz\AddonServiceProvider',
                '--tag' => 'lang'
            ]);

            $this->info('[Kompoz] lang published in resources/lang/vendor/dendev');

            $this->about_config();
        }
    }

    // Migrate
    public function migrate()
    {
        $this->info('');
        $this->info('[Kompoz] Migration: create Kompozs table');

        $this->call('migrate', []);
        $this->info('++ Done');

    }

    // Js
    public function install_js()
    {
        $this->info('');
        $this->info('[Kompoz] Js install ');

        $output=null;
        $retval=null;
        //exec('npm install kompoz --save', $output, $retval); // directly in package

        exec('npm install bootstrap --save', $output, $retval);
        exec('npm install @fortawesome/fontawesome-free --save', $output, $retval);
        exec('npm install react --save', $output, $retval);
        exec('npm install react-dom --save', $output, $retval);
        exec('npm install react-bootstrap --save', $output, $retval);
        exec('npm install react-i18next i18next --save', $output, $retval);
        exec('npm install react-autosuggest --save', $output, $retval);
        exec('npm install react-image-gallery --save', $output, $retval);

        $this->info('++ Done');
    }

    public function directories_js()
    {
        $srcs = $this->getStub();

        // kompozs
        $destination_path_dir = $this->laravel['path'] . "/../resources/js/kompozs";
        if( ! $this->files->exists($destination_path_dir) )
            mkdir($destination_path_dir);

        // ui
        $src_path_ui = $srcs['ui'];
        $destination_path_ui = $this->laravel['path'] . "/../resources/js/kompozs/ui.js";
        if( ! $this->files->exists($destination_path_ui) )
            copy($src_path_ui, $destination_path_ui);

        // styles
        $src_path_style = $srcs['styles'];
        $destination_path_style = $this->laravel['path'] . "/../resources/js/kompozs/style.css";
        if( ! $this->files->exists($destination_path_style) )
            copy($src_path_style, $destination_path_style);

        // libs dir
        $destination_path_libs = $this->laravel['path'] . "/../resources/js/kompozs/libs";
        if( ! $this->files->exists($destination_path_libs) )
            mkdir($destination_path_libs);

        // api
        $src_path_api = $srcs['api'];
        $destination_path_api = $this->laravel['path'] . "/../resources/js/kompozs/libs/KompozApi.js";
        if( ! $this->files->exists($destination_path_api) )
            copy($src_path_api, $destination_path_api);
    }

    public function install_page_controller()
    {
        $this->call('kompoz:make_page_controller', []);
    }

    // About
    public function about_config()
    {
        $configs = [
            ['MIX_KOMPOZ_API_URL', 'Url of kompoz api'],
            ['MIX_KOMPOZ_API_BEARER', 'Bearer token to acces kompoz api']
        ];
        $this->info('');
        $this->info("[Kompoz] .env" );
        $this->info('Put this in .env');

        foreach( $configs as $config )
        {
            $key = $config[0];
            $value = $config[1];

            $config_str = "{$key}=$value";
            $this->info($config_str);
        }
    }

    public function about_lang()
    {
        $this->info('');
        $this->info("[Lang] Infos" );
        $this->info("just edit files in resources/lang/vendor/dendev");
    }

    public function about_usage()
    {
        $this->info('');
        $this->info('');

        $this->info('****************************************************');
        $this->info('**                                                **');
        $this->info('** USAGE                                          **');
        $this->info('**                                                **');
        $this->info('****************************************************');

        $this->info('');
        $this->info("[Usage] Kompoz" );
        $this->info("Edit resources/views/vendor/backpack/base/inc/sidebar_content.blade.php to add : ");
        $this->line("<li class='nav-item'><a class='nav-link' href='{{ backpack_url('kompoz') }}'><i class='nav-icon la la-terminal'></i> Kompoz</a></li>");

        $this->info('');
        $this->info("[Usage] Import js" );
        $this->info("Edit resources/js/app.js");
        $this->line("require('./kompozs/ui')");

        $this->info('');
        $this->info("[Usage] Create Component" );
        $this->info("php artisan kompoz:make ComponentName Pages --blade");

        $this->info('');
        $this->info("[Usage] Page Controller");
        $this->info("Edit routes/web.php and add : ");
        $this->line("Route::get('component_name', [PageController::class, 'component_name'])->name('pages.component_name');");

        $this->info('');
        $this->info("Edit app/AHttp/Controllers/PageController and add : ");
        $this->line("
               public function component_name(Request \$request)
                {
                    return view('pages.component_name');
                }
            ");    }

    protected function getStub()
    {
        return [
            'ui' =>  __DIR__ . '/../../../stubs/ui.js',
            'styles' =>  __DIR__ . '/../../../stubs/style.css',
            'api' =>  __DIR__ . '/../../../stubs/KompozApi.js',
        ];
    }
}
