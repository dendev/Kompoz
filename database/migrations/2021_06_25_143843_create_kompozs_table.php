<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKompozsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kompozs', function (Blueprint $table) {
            $table->id();
            $table->string('label');
            $table->string('identity')->comment('unique_name_of_component');
            $table->string('description')->nullable();
            $table->text('texts')->comment('json field with all texts value of component');
           // $table->text('actions')->comment('json field with all infos about how to get datas of component');
            $table->text('styles')->comment('json field with all styles of component');
            $table->boolean('is_enabled')->default(true);
            $table->timestamps();
            // add uniq identity
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kompozs');
    }
}
