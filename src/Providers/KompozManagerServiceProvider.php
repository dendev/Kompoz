<?php

namespace Dendev\Kompoz\Providers;

use Dendev\Kompoz\Services\KompozManagerService;
use Illuminate\Support\ServiceProvider;

class ComponentManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'kompoz_manager', function ($app) {
            return new KompozManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
