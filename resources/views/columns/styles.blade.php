@php
$styles =  $entry->styles_as_array;
@endphp

@if( $styles )
    <span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <thead>
        <tr>
            <th>{{trans('dendev.kompoz::kompoz.field_style_key')}}</th>
            <th>{{trans('dendev.kompoz::kompoz.field_style_value')}}</th>
            <th>{{trans('dendev.kompoz::kompoz.field_style_description')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($styles as $style)
        <tr>
            <td>
                {{$style['key']}}
            </td>
            <td>
                <table style="width: 100%">
                    @foreach( $style['value'] as $key => $vl )
                        <tr><td>{{$key}} : {{$vl}}</td></tr>
                    @endforeach
                </table>
            </td>
            <td>
                {{$style['description']}}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </span>
@else

@endif


