@php
$actions = ( count( $entry->{$column['name']} ) > 0 ) ? $entry->{$column['name']} : false;
@endphp

@if( $actions )
    <span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <thead>
        <tr>
            <th>{{trans('dendev.kompoz::kompoz.field_action_key')}}</th>
            <th>{{trans('dendev.kompoz::kompoz.field_action_value')}}</th>
            <th>{{trans('dendev.kompoz::kompoz.field_action_description')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($actions as $action)
        <tr>
            <td>
                {{$action['key']}}
            </td>
            <td>
                {{$action['value']}}
            </td>
            <td>
                {{$action['description']}}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </span>
@else

@endif


