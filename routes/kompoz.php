<?php

/*
|--------------------------------------------------------------------------
| Dendev\Kompoz Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the Dendev\Kompoz package.
|
*/

/**
 * User Routes
 */

// Route::group([
//     'middleware'=> array_merge(
//     	(array) config('backpack.base.web_middleware', 'web'),
//     ),
// ], function() {
//     Route::get('something/action', \Dendev\Kompoz\Http\Controllers\SomethingController::actionName());
// });


/**
 * Admin Routes
 */
use Dendev\Kompoz\Http\Controllers\Api\KompozApiController;

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
], function () {
    Route::crud('kompoz', \Dendev\Kompoz\Http\Controllers\Admin\KompozCrudController::class);
});

/**
 * Api Routes
 */
Route::get('api/kompoz/{identity}/{action}/{extras?}', [KompozApiController::class, 'action'])->name('kompoz.action');
Route::post('api/kompoz/{identity}/{action}/{extras?}', [KompozApiController::class, 'action'])->name('kompoz.action');
