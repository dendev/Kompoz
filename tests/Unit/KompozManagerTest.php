<?php

namespace Tests\Unit;

use Illuminate\Support\Str;
use Orchestra\Testbench\TestCase;
use Illuminate\Auth\SessionGuard;



class KompozManagerTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Kompoz\AddonServiceProvider',
            'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            "KompozManager" => "Dendev\\Kompoz\\KompozManagerFacade",
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    //
    public function testKompozManagerExist()
    {
        $exist = \KompozManager::test_me();
        $this->assertTrue($exist);
    }

    public function testCreate()
    {
        $label = 'Test';
        $identity = Str::slug($label);
        $description = 'simple test';

        $texts = [
            ["key" => "card-title","value" => "text du titre","lang" => "fr","description" => "simple titre"],
            ["key" => "card-body-label","value" => "balablablab","lang" => "fr","description" =>"je sais pas"]
        ];

        $actions = [
            ["key" => "get","value" => "Tests\\Unit\\Getter::get_1","description" => "test"]
        ];

        $styles = [
            ["key" => "card-body__background","value" => ["backgroundColor" => "blue", "color" => "white"],"description" => "color"]
        ];

        $is_enabled = true;

        $kompoz = \KompozManager::create($label, $texts, $actions, $styles, $identity, $description, $is_enabled);
        $this->assertEquals($label, $kompoz->label);
        $this->assertEquals($identity, $kompoz->identity);
        $this->assertEquals($description, $kompoz->description);
        $this->assertEquals($texts, $kompoz->texts);
        $this->assertEquals($actions, $kompoz->actions);
        $this->assertEquals($styles, $kompoz->styles);
        $this->assertEquals($is_enabled, $kompoz->is_enabled);
    }

    public function testDoubleCreate()
    {
        $k1 = $this->_make_default_kompoz();
        $k2 = $this->_make_default_kompoz('Test', ['test']);

        $k1->refresh(); // refresh from db

        $this->assertEquals($k1->label, $k2->label);
        $this->assertEquals($k1->texts, ['test']); // updated
        $this->assertEquals($k2->texts, ['test']); // is same instance then k1
    }

    public function testDoActionOfKompoz()
    {
        $k1 = $this->_make_default_kompoz();

        $datas = \KompozManager::do_action_of_kompoz($k1, 'get',[]);
        $this->assertEquals(['field_a' => 'a'], $datas);
    }


    public function testRunKompozByIdentity()
    {
        $k1 = $this->_make_default_kompoz();

        $datas = \KompozManager::run('test', 'get',[]);
        $this->assertEquals('Test', $datas['label']);
        $this->assertEquals('test', $datas['identity']);
    }

    // -
    private function _make_default_kompoz($label = false, $texts = false, $actions = false, $styles = false)
    {
        if( ! $texts )
            $texts = [
                ["key" => "card-title","value" => "text du titre","lang" => "fr","description" => "simple titre"],
                ["key" => "card-body-label","value" => "balablablab","lang" => "fr","description" =>"je sais pas"]
            ];

        if( ! $actions )
            $actions = [
                ["key" => "get","value" => "Tests\\Unit\\Getter::get_1","description" => "test"]
            ];

        if( ! $styles )
            $styles = [
                ["key" => "card-body__background","value" => ["backgroundColor" => "blue", "color" => "white"],"description" => "color"]
            ];
        /*
        if( ! $texts )
        $texts = [
            'en' => [
                'glyph' => [
                    "glyph-card__body__title" => "Welcome test",
                    "glyph-card__body__text" => "to React and react-i18next FROM API",
                    "glyph-card__body__button-label" => "Ok",
                ]
            ],
            'fr' => [
                'glyph' => [
                    "glyph-card__body__text" => "Welcome to React and react-i18next FROM API"
                ]
            ]
        ];

        if( ! $actions )
            $actions = [
                'get' => '\Tests\Unit\Getter::get_1'
            ];

        if( ! $styles )
            $styles = [
                'glyph-card' => [ 'backgroundColor' => 'blue', 'color' => 'white' ],
                'glyph-card__body' => null,
                'glyph-card__body__title' =>  [ 'color' => 'green'],
                'glyph-card__body__text' => [ 'color' =>  'black'],
                'glyph-card__body__button' =>  null,
            ];
            */
        if( ! $label)
            $label = 'Test';

        $identity = Str::slug($label);
        $description = 'simple test';
        $is_enabled = true;

        $kompoz = \KompozManager::create($label, $texts, $actions, $styles, $identity, $description, $is_enabled);
        return $kompoz;
    }
}

class Getter
{
    public function get_1()
    {
        // do complex action to get actual value
        return ['field_a' => 'a'];
    }
}
