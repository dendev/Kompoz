<?php

namespace Dendev\Kompoz\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Dendev\Kompoz\Http\Requests\KompozRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Str;

/**
 * Class KompozCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class KompozCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;// { CreateOperation::store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;// { UpdateOperation::update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Dendev\Kompoz\Http\Controllers\Admin\Operations\ShowWebOperation;
    use \Dendev\Kompoz\Http\Controllers\Admin\Operations\ShowApiOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\Dendev\Kompoz\Models\Kompoz::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/kompoz');
        CRUD::setEntityNameStrings('kompoz', 'kompozs');

        CRUD::allowAccess('show_web');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_label') ),
            'name' => 'label',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_description') ),
            'name' => 'description',
            'type' => 'textarea',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_is_enabled') ),
            'name' => 'is_enabled',
            'type' => 'boolean',
            'options' => [
                0 => ucfirst( trans('dendev.kompoz::kompoz.field_is_enabled_no') ),
                1 => ucfirst( trans('dendev.kompoz::kompoz.field_is_enabled_yes') ),
            ]
        ]);
    }

    protected function setupShowOperation()
    {
        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_label') ),
            'name' => 'label',
            'type' => 'text',
        ]);


        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_identity') ),
            'name' => 'identity',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_description') ),
            'name' => 'description',
            'type' => 'textarea',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_texts') ),
            'name' => 'texts',
            'type' => 'view',
            'view'  => 'dendev.kompoz::columns.texts',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_actions') ),
            'name' => 'actions',
            'type' => 'view',
            'view'  => 'dendev.kompoz::columns.actions',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_styles') ),
            'name' => 'styles',
            'type' => 'view',
            'view'  => 'dendev.kompoz::columns.styles',
        ]);


        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_is_enabled') ),
            'name' => 'is_enabled',
            'type' => 'boolean',
            'options' => [
                0 => ucfirst( trans('dendev.kompoz::kompoz.field_is_enabled_no') ),
                1 => ucfirst( trans('dendev.kompoz::kompoz.field_is_enabled_yes') ),
            ]
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(KompozRequest::class);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_label') ),
            'name' => 'label',
            'type' => 'text',
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_description') ),
            'name' => 'description',
            'type' => 'textarea',
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_texts') ),
            'name' => 'texts',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'key',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_text_label') ),
                    'wrapper' => ['class' => 'form-group col-md-5'],
                ],
                [
                    'name'    => 'value',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_text_value') ),
                    'wrapper' => ['class' => 'form-group col-md-5'],
                ],
                [
                    'name'    => 'lang',
                    'type'    => 'select2_from_array',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_text_lang') ),
                    'options'     => ['fr' => 'Fr', 'en' => 'En', 'epo' => 'Esperanto'],
                    'allows_null' => false,
                    'default'     => 'fr',
                    'wrapper' => ['class' => 'form-group col-md-2'],
                ],
                [
                    'name'  => 'description',
                    'type'  => 'textarea',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_text_description') ),
                ],
            ],
            // optional
            'new_item_label'  => ucfirst( trans('dendev.kompoz::kompoz.field_text_add') ),
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_actions') ),
            'name' => 'actions',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'key',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_action_key') ),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'value',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_action_value') ),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'  => 'description',
                    'type'  => 'textarea',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_action_description') ),
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
            ],
            // optional
            'new_item_label'  => ucfirst( trans('dendev.kompoz::kompoz.field_action_add') ),
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_styles') ),
            'name' => 'styles',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'key',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_style_key') ),
                ],
                [
                    'name'    => 'value',
                    'type'    => 'textarea',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_style_value') ),
                ],
                [
                    'name'  => 'description',
                    'type'  => 'textarea',
                    'label'   => ucfirst( trans('dendev.kompoz::kompoz.field_style_description') ),
                ],
            ],
            // optional
            'new_item_label'  => ucfirst( trans('dendev.kompoz::kompoz.field_style_add') ),
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.kompoz::kompoz.field_is_enabled') ),
            'name' => 'is_enabled',
            'type' => 'boolean',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
/*
    public function store()
    {
        $this->crud->hasAccessOrFail('create');
        return $this->_store_and_upate();
    }

    public function update()
    {
        $this->crud->hasAccessOrFail('update');
        return $this->_store_and_upate($is_udpate = true);
    }

    private function _store_and_upate($is_update = false)
    {
        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        // get datas
        $datas =  $request->all();

        $label = $datas['label'];
        $description = ( $datas['description'] ) ? $datas['description'] : null;
        $texts = $datas['texts'];
        $actions = $datas['actions'];
        $styles = $datas['styles'];
        $is_enabled = $datas['is_enabled'];

        $identity = Str::slug($label);

        // format texts
        $texts_fromated = json_decode($texts, true);

        // format actions
        $actions_formated = json_decode($actions, true);

        // format styles
        $tmp_styles = json_decode($styles, true);
        $styles_formated = [];
        foreach( $tmp_styles as $tmp_style)
        {
            if( array_key_exists('value', $tmp_style))
            {
               if( is_string($tmp_style['value'] ) )
               {
                   $value_formated = [];
                   $values = explode(',', $tmp_style['value']);
                   foreach( $values as $value)
                   {
                       $vl = explode(':', $value);
                       if( count( $vl) === 2 )
                       {
                           $key = trim($vl[0]);
                           $css = trim($vl[1]);
                           $value_formated[$key] = $css;
                       }
                   }
                   $tmp_style['value'] = $value_formated;
               }
            }
            $styles_formated[] = $tmp_style;
        }

        // create
        $kompoz = \KompozManager::create($label, $texts_fromated, $actions_formated, $styles_formated, $identity, $description, $is_enabled);

        // for backpack
        $this->data['entry'] = $this->crud->entry = $kompoz; // utils ??

        // inform
        if( $kompoz )
            \Alert::success(trans('backpack::crud.insert_success'))->flash();
        else
            \Alert::error(trans('dendev.kompoz::kompoz.insert_error'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($kompoz->id);
    }
*/
}
