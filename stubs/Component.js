import React from "react";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Kompoz from '../../../../vendor/dendev/kompoz/resources/assets/js/Kompoz';
import { Card, Button} from "react-bootstrap";
//import './identity.css';

/*
 * Usage
 * use this.gtoc('key') to get text
 * use this.gvod('key') to get data
 * use this.gsoc('key') to get style
 *
 * Convention
 * @ref bem notations https://www.alsacreations.com/article/lire/1641-Bonnes-pratiques-en-CSS--BEM-et-OOCSS.html
 *
 * Texts
 *
 * Datas
 *
 * Styles
 *
 */


export default class Component extends Kompoz
{
    constructor(props)
    {
        super(props);

        this.state = {};
    }

    render_component()
    {
        return (
            <Card className={'identity-card'} style={this.gsoc('identity-card')}>
                <Card.Img variant="top" src={this.gvod('identity-card__img__src')} />
                <Card.Body className={'identity-card__body'} style={this.gsoc('identity-card__body')}>
                    <Card.Title className={'identity-card__body__title'} style={this.gsoc('identity-card__body__title')}>{this.gtoc('identity-card__body__title')}</Card.Title>
                    <Card.Text className={'identity-card__body__text'} style={this.gsoc('identity-card__body__text')}>
                        {this.gtoc('identity-card__body__text')}
                    </Card.Text>
                    <Button variant="primary" className={'identity-card__body__button'} style={this.gsoc('identity-card__body__button')}>{this.gtoc('identity-card__body__button-label')}</Button>
                </Card.Body>
            </Card>
        );
    }
}

Component.propTypes = {
    /**
     * Name of component
     */
    label: PropTypes.string,

    /**
     * Slug name of component
     * used for dialog with api
     */
    identity: PropTypes.string,

    /**
     * Some infos about component
     */
    description: PropTypes.string,

    /**
     * object provide display fixed texts
     */
    texts: PropTypes.object,

    /**
     * object provide datas
     */
    datas: PropTypes.object,

    /**
     * object with css class and values
     */
    styles: PropTypes.object,
};

Component.defaultProps = {
    texts: {
    },
    datas: {},
    styles: {},
    settings: {
        component_name: 'identity',
        providers: {
            is_same_provider: true,
            is_enabled: true,
            texts: {
                rest_api : process.env.MIX_KOMPOZ_API_URL + 'identity/get',
                bearer_token: process.env.MIX_KOMPOZ_API_BEARER,
            },
            datas: {
                rest_api : process.env.MIX_KOMPOZ_API_URL + 'identity/get',
                bearer_token: process.env.MIX_KOMPOZ_API_BEARER,
            },
            styles: {
                rest_api : process.env.MIX_KOMPOZ_API_URL + 'identity/get',
                bearer_token: process.env.MIX_KOMPOZ_API_BEARER,
            }
        },
        debug: true,
    },
};

