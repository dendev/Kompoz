<?php

namespace Dendev\Kompoz\Http\Controllers\Admin\Operations;

use Dendev\Kompoz\Models\Kompoz;
use Illuminate\Support\Facades\Route;

trait ShowWebOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupShowWebRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/show_web/{kompoz_id}', [
            'as'        => $routeName.'.show_web',
            'uses'      => $controller.'@show_web',
            'operation' => 'show_web',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupShowWebDefaults()
    {
        //$this->crud->allowAccess('show_web');

        $this->crud->operation('show_web', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'show_web', 'view', 'dendev.kompoz::buttons.show_web');
        });

        $this->crud->operation('show', function () {
            $this->crud->addButton('line', 'show_web', 'view', 'dendev.kompoz::buttons.show_web');
        });
    }

    public function show_web($kompoz_id)
    {
        $kompoz = Kompoz::find($kompoz_id);// route('kompoz.show_web', ['kompoz_id' => $kompoz_id]);

        if( view()->exists('pages/'.$kompoz->identity))
        {
            $url = "/" . $kompoz->identity;
        }
        else
        {
            $url = route('kompoz.index');
            \Alert::warning(trans('dendev.kompoz::kompoz.operation_show_web_no'))->flash();
        }

        return \Redirect::to($url);
    }


}
