<?php

namespace Dendev\Kompoz\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class MakePageController extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kompoz:make_page_controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create a controller for pages component';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../../../stubs/PageController.php';
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {
        $this->_make_controller();
    }

    private function _make_controller()
    {
        $this->info('');
        $this->info('* Make Controller');

        $destination_path = $this->laravel['path'] . "/Http/Controllers/PageController.php";

        if ($this->files->exists($destination_path)) {
            $this->error('-- PageController already exists!');

            return false;
        }

        $this->makeDirectory($destination_path);

        $this->files->put($destination_path, $this->buildFile());

        $this->info("++ PageController created successfully.");
        $this->info("?? Edit app/AHttp/Controllers/PageController and add : ");
        $this->line("
               public function component_name(Request \$request)
                {
                    return view('pages.component_name');
                }
            ");

        $this->info('');
        $this->info("?? Edit web.php and add : ");
        $this->line("Route::get('component_name', [PageController::class, 'component_name'])->name('pages.component_name');");
    }

    protected function buildFile()
    {
        $stub = $this->files->get($this->getStub());

        return $this->makeReplacements($stub);
    }

    protected function makeReplacements(&$stub)
    {
        // nothing now
        return $stub;
    }
}
