<?php
?>
<a href="javascript:void(0)" onclick="showWeb(this)"
   data-route="{{route('kompoz.show_web', ['kompoz_id' => $entry->id])}}"
   data-button-type="show_web"
   class="btn btn-sm btn-link">
    <i class="la la-html5"></i> {{trans('dendev.kompoz::kompoz.show_web_action')}}
</a>

<script>
    if (typeof showWeb != 'function') {
        var fct = document.querySelector("[data-button-type=show_web]")
        document.removeEventListener('click', fct);

        function showWeb(button) {
            console.log('test');
            var button = $(button);
            var route = button.attr('data-route');

            console.log( route);
            window.location.href = route;
        }
    }
    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('deleteEntry');
</script>
