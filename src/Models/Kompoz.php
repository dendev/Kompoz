<?php

namespace Dendev\Kompoz\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Kompoz extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'kompozs';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getTextsAttribute($value)
    {
        if( $value )
            $value = json_decode($value, true);

        return $value;
    }

    public function getActionsAttribute($value)
    {
        if( $value )
            $value = json_decode($value, true);

        return $value;
    }

    public function getStylesAttribute($value)
    {
        if( $value )
            $value = json_decode($value, true);

        return $value;
    }


    public function getStylesAsArrayAttribute($value) // specially for backpack crud column
    {
        $tmp_styles = json_decode($this->attributes['styles'], true);
        $styles_formated = [];
        foreach( $tmp_styles as $tmp_style)
        {
            if( array_key_exists('value', $tmp_style))
            {
                if( is_string($tmp_style['value'] ) )
                {
                    $value_formated = [];
                    $values = explode(',', $tmp_style['value']);
                    foreach( $values as $value)
                    {
                        $vl = explode(':', $value);
                        if( count( $vl) === 2 )
                        {
                            $key = trim($vl[0]);
                            $css = trim($vl[1]);
                            $value_formated[$key] = $css;
                        }
                    }
                    $tmp_style['value'] = $value_formated;
                }
            }
            $styles_formated[] = $tmp_style;
        }

        return $styles_formated;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setTextsAttribute($value)
    {
        if( is_array($value))
            $this->attributes['texts'] = json_encode($value);
        else
            $this->attributes['texts'] = $value;
    }

    public function setActionsAttribute($value)
    {
        if( is_array($value))
            $this->attributes['actions'] = json_encode($value);
        else
            $this->attributes['actions'] = $value;
    }

    public function setStylesAttribute($value)
    {
        if( is_array($value))
            $this->attributes['styles'] = json_encode($value);
        else
            $this->attributes['styles'] = $value;
    }

    /*
      |--------------------------------------------------------------------------
      | Events
      |--------------------------------------------------------------------------
      */
    protected static function booted()
    {
        static::saving(function ($kompoz) {
            if( is_null($kompoz->identity) )
                $kompoz->attributes['identity'] = Str::slug($kompoz->label);
        });
    }
}
